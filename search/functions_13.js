var searchData=
[
  ['uncompress_5fbuf',['uncompress_buf',['../namespacetwist_1_1zlib.html#a3d5cf9cc88c73438a99ec05693f408a9',1,'twist::zlib']]],
  ['underscore_5fchar',['underscore_char',['../namespacetwist.html#a14a51b50f9724f0314ad78a69b4724dd',1,'twist']]],
  ['undo_5fand_5fremove_5flast_5fcommand',['undo_and_remove_last_command',['../classtwist_1_1_command_mgr.html#a8805e13816cde9f8c0cbd5c06fec3768',1,'twist::CommandMgr']]],
  ['undo_5fit',['undo_it',['../classtwist_1_1_command.html#a1211fea25d73c250f49c55647f80a9d8',1,'twist::Command']]],
  ['unique',['unique',['../namespacetwist.html#a2b893865333a80bb58a08b2d7991dc19',1,'twist::unique(Rng &amp;range)'],['../namespacetwist.html#a0900e1e7ce5fb74180f413ff5f2a9b65',1,'twist::unique(Rng &amp;range, BinaryPred pred)']]],
  ['unit',['unit',['../classtwist_1_1_chronometer.html#a247c8c097d72b1fe785831279e6c5d65',1,'twist::Chronometer::unit()'],['../classtwist_1_1gis_1_1_erdas_imagine_file_1_1_map_info.html#a43cdf8c9eef23ff5ed5bc3428886ed07',1,'twist::gis::ErdasImagineFile::MapInfo::unit()']]],
  ['unite',['unite',['../namespacetwist_1_1math.html#a9ae984e1732f838d7aa092fb639870c0',1,'twist::math']]],
  ['univardistrsample',['UnivarDistrSample',['../classtwist_1_1math_1_1_univar_distr_sample.html#a1665943d00ce0d05dc8c35929d4d8e91',1,'twist::math::UnivarDistrSample::UnivarDistrSample(Ssize size)'],['../classtwist_1_1math_1_1_univar_distr_sample.html#a46d9b2126940c0b7c8ef9f02ca0fb7c0',1,'twist::math::UnivarDistrSample::UnivarDistrSample(Iter sample_begin, Iter sample_end)'],['../classtwist_1_1math_1_1_univar_distr_sample.html#a1ab9a6641b3a280b2bceed9381e5d5f6',1,'twist::math::UnivarDistrSample::UnivarDistrSample(std::unique_ptr&lt; Sam[]&gt; buffer, Ssize buffer_size)']]],
  ['univardistrsampleexaminer',['UnivarDistrSampleExaminer',['../classtwist_1_1unn_1_1_univar_distr_sample_examiner.html#a44a5e58369d1d25ed4c5e64ef0a9d267',1,'twist::unn::UnivarDistrSampleExaminer']]],
  ['univardistrsampleinfo',['UnivarDistrSampleInfo',['../classtwist_1_1math_1_1_univar_distr_sample_info.html#a08f585771a71b56e7d033d2d57f01f72',1,'twist::math::UnivarDistrSampleInfo::UnivarDistrSampleInfo(const UnivarDistrSampleInfo &amp;src)'],['../classtwist_1_1math_1_1_univar_distr_sample_info.html#a7bd8d7270d82b34ccaed62c618227c85',1,'twist::math::UnivarDistrSampleInfo::UnivarDistrSampleInfo(UnivarDistrSampleInfo &amp;&amp;src)']]],
  ['unselect_5fall',['unselect_all',['../namespacetwist_1_1mfc.html#a74a3fc75d5d5e8262b8b537627b79819',1,'twist::mfc']]],
  ['updatemaxcolcontentwidth',['UpdateMaxColContentWidth',['../classtwist_1_1mfc_1_1_ctrl_list_owner_draw_base.html#a7cdc74be66d0b8e33e35c22b67697fd0',1,'twist::mfc::CtrlListOwnerDrawBase']]],
  ['upper_5fbound',['upper_bound',['../namespacetwist.html#abe57ba656f87fb3acd30d97e31790509',1,'twist']]],
  ['user_5fto_5finternal_5fdata',['user_to_internal_data',['../namespacetwist_1_1mfc_1_1detail.html#a58fe13de070a73dc10b4c2ef38b2a4f0',1,'twist::mfc::detail::user_to_internal_data(Data data, std::false_type is_ptr)'],['../namespacetwist_1_1mfc_1_1detail.html#afaea93171622afe5cfdfa505801703f7',1,'twist::mfc::detail::user_to_internal_data(Data data, std::true_type is_ptr)']]],
  ['utc_5ftimepoint_5fto_5fdate',['utc_timepoint_to_date',['../namespacetwist.html#a7ee8769a08777481f7e85a772127164f',1,'twist']]],
  ['utc_5ftimepoint_5fto_5fdatetime',['utc_timepoint_to_datetime',['../namespacetwist.html#a91cc9ba01a93a4474c8d53cd8607a0fd',1,'twist']]],
  ['uuid',['Uuid',['../classtwist_1_1_uuid.html#a941c6a88ee33388cdcf81145093ed8ee',1,'twist::Uuid::Uuid()'],['../classtwist_1_1_uuid.html#a3a98854f8467afa4d981857615cad514',1,'twist::Uuid::Uuid(const std::wstring &amp;uuid_str)'],['../classtwist_1_1_uuid.html#a4cf2a0b85d504124641f19e9b4f1821d',1,'twist::Uuid::Uuid(const Uuid &amp;src)'],['../classtwist_1_1_uuid.html#a9be5bf764be5b5b25584e6d3ae7ee617',1,'twist::Uuid::Uuid(Uuid &amp;&amp;src)']]],
  ['uuidobjectmap',['UuidObjectMap',['../classtwist_1_1_uuid_object_map.html#a23d62cd948c320b68edebba8fdeaf6e6',1,'twist::UuidObjectMap']]]
];
