var searchData=
[
  ['colour_5finterpretation_5fname',['colour_interpretation_name',['../structtwist_1_1gis_1_1_raster_dataset_info_1_1_raster_band_info.html#a8f24157e5207f79dd793a919ee49d559',1,'twist::gis::RasterDatasetInfo::RasterBandInfo']]],
  ['colour_5ftable_5fentry_5fcount',['colour_table_entry_count',['../structtwist_1_1gis_1_1_raster_dataset_info_1_1_raster_band_info.html#ac6b73ab2398d9b4be85e4f1794984ae0',1,'twist::gis::RasterDatasetInfo::RasterBandInfo']]],
  ['create',['create',['../classtwist_1_1db_1_1netcdf_1_1_netcdf_file.html#a4424343419cd98c0cc166b6a55874e5b',1,'twist::db::netcdf::NetcdfFile::create()'],['../classtwist_1_1db_1_1_sqlite_database_base.html#ad05991aa23a8afcd93fea277647f2c4a',1,'twist::db::SqliteDatabaseBase::create()'],['../classtwist_1_1gis_1_1_erdas_imagine_file.html#a847f0297be8bd440856b6ad0d8fb1b6c',1,'twist::gis::ErdasImagineFile::create()']]]
];
