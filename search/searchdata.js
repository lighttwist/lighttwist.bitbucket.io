var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz~",
  1: "abcdefghiklmnopqrstuvwx",
  2: "st",
  3: "abcdefghiklmnopqrstuvwxz",
  4: "abcdefghiklmnopqrstuvwxyz~",
  5: "abcdefghikmnoprstwxy",
  6: "acdefghiklmnoprstuvwx",
  7: "acdefghkmnopqrstuw",
  8: "abcdefghijklmnoprstuvwyz",
  9: "mst",
  10: "_gnpqstvw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros"
};

