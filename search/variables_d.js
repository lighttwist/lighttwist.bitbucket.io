var searchData=
[
  ['params',['params',['../structtwist_1_1gis_1_1_erdas_imagine_file_1_1_map_info_1_1_datum.html#ad38ec081f71c9c56fba2f6878e1516bb',1,'twist::gis::ErdasImagineFile::MapInfo::Datum']]],
  ['pixel_5fsize',['pixel_size',['../structtwist_1_1gis_1_1_raster_geo_info.html#a49ea3474cf1e0e8f9b47e3bb4b73e41e',1,'twist::gis::RasterGeoInfo']]],
  ['proj_5fexe_5fname',['proj_exe_name',['../structtwist_1_1gis_1_1_erdas_imagine_file_1_1_map_info_1_1_projection_params.html#acd460831fe9e713e7ce09af8f3ff2db6',1,'twist::gis::ErdasImagineFile::MapInfo::ProjectionParams']]],
  ['proj_5fname',['proj_name',['../structtwist_1_1gis_1_1_erdas_imagine_file_1_1_map_info_1_1_projection_params.html#a1b987942389863c52e7a2804140f9813',1,'twist::gis::ErdasImagineFile::MapInfo::ProjectionParams']]],
  ['proj_5fnumber',['proj_number',['../structtwist_1_1gis_1_1_erdas_imagine_file_1_1_map_info_1_1_projection_params.html#aaddda2a15a5d89eea4b3ef1825a8cc15',1,'twist::gis::ErdasImagineFile::MapInfo::ProjectionParams']]],
  ['proj_5fparams',['proj_params',['../structtwist_1_1gis_1_1_erdas_imagine_file_1_1_map_info_1_1_projection_params.html#a46a319c8df62e77ba9f243cdd7419dc6',1,'twist::gis::ErdasImagineFile::MapInfo::ProjectionParams']]],
  ['proj_5fspheroid',['proj_spheroid',['../structtwist_1_1gis_1_1_erdas_imagine_file_1_1_map_info_1_1_projection_params.html#a8a73e03e113d3763f5a93c57d1daa8ff',1,'twist::gis::ErdasImagineFile::MapInfo::ProjectionParams']]],
  ['proj_5ftype',['proj_type',['../structtwist_1_1gis_1_1_erdas_imagine_file_1_1_map_info_1_1_projection_params.html#aaad27cc3d62de390028d2bf54ace8bbb',1,'twist::gis::ErdasImagineFile::MapInfo::ProjectionParams']]],
  ['proj_5fzone',['proj_zone',['../structtwist_1_1gis_1_1_erdas_imagine_file_1_1_map_info_1_1_projection_params.html#afb487cf0e84c1e9ae40a6b02e90d983f',1,'twist::gis::ErdasImagineFile::MapInfo::ProjectionParams']]],
  ['projection',['projection',['../structtwist_1_1gis_1_1_raster_dataset_info.html#a4594b067a14ec5af998497bc196e07e7',1,'twist::gis::RasterDatasetInfo']]]
];
