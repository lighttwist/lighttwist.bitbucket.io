var searchData=
[
  ['data_5fvalue_5ftype',['data_value_type',['../structtwist_1_1gis_1_1_raster_dataset_info_1_1_raster_band_info.html#a178908498a4569f8c5102c6549b44886',1,'twist::gis::RasterDatasetInfo::RasterBandInfo']]],
  ['datum',['datum',['../structtwist_1_1gis_1_1_erdas_imagine_file_1_1_map_info_1_1_projection_params.html#afff49632b8fd2eed93dc896e141d30f6',1,'twist::gis::ErdasImagineFile::MapInfo::ProjectionParams']]],
  ['datum_5fname',['datum_name',['../structtwist_1_1gis_1_1_erdas_imagine_file_1_1_map_info_1_1_datum.html#aecf62ce346c084710be51c13978e5a56',1,'twist::gis::ErdasImagineFile::MapInfo::Datum']]],
  ['dim_5fcount',['dim_count',['../classtwist_1_1db_1_1netcdf_1_1_hyperslab_and_dim_data.html#a95421d7e95f70588f9443a79573868d6',1,'twist::db::netcdf::HyperslabAndDimData::dim_count()'],['../classtwist_1_1db_1_1netcdf_1_1_hyperslab_and_dim_data_reader.html#a4009f8e41552ee69d94e0bf422d12c2f',1,'twist::db::netcdf::HyperslabAndDimDataReader::dim_count()'],['../classtwist_1_1db_1_1netcdf_1_1_hyperslab_and_dim_params.html#aaa3907dbba4fe6968ea18cf8ef1ca305',1,'twist::db::netcdf::HyperslabAndDimParams::dim_count()']]],
  ['driver_5flong_5fname',['driver_long_name',['../structtwist_1_1gis_1_1_raster_dataset_info.html#a3fc9df97922b57c42a5589b0fc0db17f',1,'twist::gis::RasterDatasetInfo']]],
  ['driver_5fname',['driver_name',['../structtwist_1_1gis_1_1_raster_dataset_info.html#ac788c32373d37bbddec5dafeb7e50e2d',1,'twist::gis::RasterDatasetInfo']]]
];
