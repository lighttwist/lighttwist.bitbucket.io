var searchData=
[
  ['b',['b',['../structtwist_1_1gis_1_1_erdas_imagine_file_1_1_map_info_1_1_projection_spheroid.html#a26d58111999ec230dd1d2bb3a8d46acc',1,'twist::gis::ErdasImagineFile::MapInfo::ProjectionSpheroid']]],
  ['band_5fcount',['band_count',['../structtwist_1_1gis_1_1_raster_dataset_info.html#a9d101ede2c2e18f5cd6ba34c3d6cd0f5',1,'twist::gis::RasterDatasetInfo']]],
  ['bands_5finfo',['bands_info',['../structtwist_1_1gis_1_1_raster_dataset_info.html#acf8db3950a653941c4fb713a7341aa8c',1,'twist::gis::RasterDatasetInfo']]],
  ['block_5fheight',['block_height',['../structtwist_1_1gis_1_1_raster_dataset_info_1_1_raster_band_info.html#a4965e2f5a69b40b4cf7352b59887bbb4',1,'twist::gis::RasterDatasetInfo::RasterBandInfo']]],
  ['block_5fwidth',['block_width',['../structtwist_1_1gis_1_1_raster_dataset_info_1_1_raster_band_info.html#ac069da5482f1880f16dc5e8ce681104e',1,'twist::gis::RasterDatasetInfo::RasterBandInfo']]],
  ['bn_5ffunc_5fnode_5ftype',['bn_func_node_type',['../namespacetwist_1_1unn.html#a5e5ba691fe9d332eec1ed52844015c2e',1,'twist::unn']]],
  ['bn_5fprob_5fnode_5ftype',['bn_prob_node_type',['../namespacetwist_1_1unn.html#ad4c4b9e4ad8a3fcb4dd5817e88606ac4',1,'twist::unn']]]
];
